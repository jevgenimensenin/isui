import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class MainTest {

    @Test
    public void userCanLoginByUsername() {

        for (int i = 0; i < 10000; i++) {
            open("http://localhost:8000/");
            $(By.name("age")).setValue(String.valueOf(new Random().nextInt(100)));
            clickRadio("energy");
            clickRadio("social_group");
            clickRadio("social");
            clickRadio("car");
            clickRadio("life_pace");
            clickRadio("rel_status");
            $(By.name("num_of_kids")).setValue(generateNumOfKids());
            clickRadio("sport");
            clickRadio("subject");
            clickRadio("committed");
            clickRadio("patience");
            $(By.name("submit")).click();
            Path path = Paths.get("result.arff");
            String resultString = $(By.id("inputParams")).val() + "\n";
            try {
                Files.write(path, resultString.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String generateNumOfKids() {
        int random = new Random().nextInt(3);
        if (random == 0) {
            return String.valueOf(0);
        } else if(random == 1){
            return String.valueOf(new Random().nextInt(3 - 1) + 1);
        } else {
            return String.valueOf(3);
        }
    }

    public void clickRadio(String elementname) {
        List<WebElement> radios = getWebDriver().findElements(By.name(elementname));
        int option = new Random().nextInt(radios.size());
        radios.get(option).click();
    }
}